package modelo;

public class Grupo {

	private int codigo;
	private String grupo;

	public Grupo() {

	}

	public Grupo(int codigo, String grupo) {
		this.codigo = codigo;
		this.grupo = grupo;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

}

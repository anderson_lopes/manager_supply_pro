package modelo;

public class Produto {

	private int codigo;
	private String produto;
	private double estoqueMinimo;
	private double estoqueMaximo;
	private String apresentacao;
	private double preco;
	private Grupo grupo;
	private double saldo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public double getEstoqueMinimo() {
		return estoqueMinimo;
	}

	public void setEstoqueMinimo(double estoqueMinimo) {
		this.estoqueMinimo = estoqueMinimo;
	}

	public double getEstoqueMaximo() {
		return estoqueMaximo;
	}

	public void setEstoqueMaximo(double estoqueMaximo) {
		this.estoqueMaximo = estoqueMaximo;
	}

	public String getApresentacao() {
		return apresentacao;
	}

	public void setApresentacao(String apresentacao) {
		this.apresentacao = apresentacao;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

}

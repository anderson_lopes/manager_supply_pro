package service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import modelo.Grupo;

public class GrupoService {

	private List<Grupo> grupos;
	protected Scanner scanner;
	protected Grupo grupo;

	public void cadastraGrupo() {
		this.scanner = new Scanner(System.in);
		this.grupo = new Grupo();
		System.out.print("Descri��o do grupo: ");
		grupo.setGrupo(scanner.nextLine().toUpperCase());
		adiciona(grupo);
		this.scanner = null;
		System.out.println("Grupo cadastrado com sucesso!");
	}

	public void editaGrupo() {
		this.scanner = new Scanner(System.in);
		this.listarGrupos();
		System.out.print("Digite o c�digo do grupo que deseja editar:");
		Grupo grupotemp = obterGrupoPeloCodigo(scanner.nextInt());
		if (grupotemp != null) {
			System.out.println("Digite a nova descri��o do grupo " + grupotemp.getGrupo());
			this.scanner = new Scanner(System.in);
			String temp = scanner.nextLine();
			if (!temp.trim().equals("")) {
				getGrupos().get(grupotemp.getCodigo() - 1).setGrupo(temp.toUpperCase());
				System.out.println("Grupo alterado com sucesso");
			}
		} else {
			System.out.println("N�o foi possivel localizar/alterar grupo!");
		}
		this.scanner = null;
	}

	public void removeGrupo() {
		this.scanner = new Scanner(System.in);
		this.listarGrupos();
		System.out.println("Digite o c�digo do grupo que deseja remover:");
		this.remover(this.obterGrupoPeloCodigo(scanner.nextInt()));
		this.scanner = null;
		System.out.println("Grupo removido com sucesso!");
	}

	public void remover(Grupo grupo) {
		this.getGrupos().get(this.obterCodigoPeloGrupo(grupo)).setCodigo(0);
	}

	public void adiciona(Grupo grupo) {
		grupo.setCodigo(this.getGrupos().size() + 1);
		this.getGrupos().add(grupo);
	}

	public int obterCodigoPeloGrupo(Grupo grupo) {
		for (int i = 0; i < grupos.size(); i++) {
			if (grupo.getCodigo() == grupos.get(i).getCodigo()) {
				return i;
			}
		}
		return 0;
	}

	public Grupo obterGrupoPeloCodigo(int codigo) {
		for (int i = 0; i < this.getGrupos().size(); i++) {
			if (codigo == this.getGrupos().get(i).getCodigo()) {
				return this.getGrupos().get(i);
			}
		}
		return null;
	}

	public List<Grupo> getGrupos() {
		if (grupos == null) {
			grupos = new ArrayList<Grupo>();
		}
		return grupos;
	}

	public void listarGrupos() {
		if (this.grupos != null) {
			System.out.println("=========================");
			System.out.println("GRUPOS CADASTRADOS");
			System.out.println("=========================");
			for (Grupo g : grupos) {
				if (g.getCodigo() > 0) {
					System.out.println("C�digo: " + g.getCodigo() + "\tDescri��o: " + g.getGrupo());
				}
			}
			System.out.println("=========================");
		} else {
			System.out.println("Nenhum grupo de produto cadastrado!");
		}
	}
}
package service;

import java.util.Scanner;

import modelo.Produto;

public class PedidoService {

	protected Scanner scanner;

	protected ProdutoService produtoService;

	public void executaPedido(ProdutoService produtoService, char tipo) {
		this.produtoService = produtoService;
		if (tipo == 'E') {
			produtoService.atualizaProduto(entrada());
		} else {
			produtoService.atualizaProduto(saida());
		}
	}

	public Produto entrada() {
		scanner = new Scanner(System.in);
		produtoService.listarProdutos();
		System.out.print("Digite o c�digo do produto da lista para efetuar entrada: ");
		Produto produto = produtoService.obterProdutoPeloCodigo(scanner.nextInt());
		System.out.print("Digite a quantidade do produto: ");
		int qtde = scanner.nextInt();
		produto.setSaldo(produto.getSaldo() + qtde);
		if (produto.getSaldo() > produto.getEstoqueMaximo()) {
			System.out.println("Estoque m�ximo de " + produto.getEstoqueMaximo() + " execedido em "
					+ (produto.getSaldo() - produto.getEstoqueMaximo()) + " " + produto.getApresentacao() + "S");
		}
		System.out.print("Digite o valor unitario do produto: ");
		produto.setPreco(scanner.nextDouble());
		System.out.println("Entrada efetuada com sucesso!");
		return produto;
	}

	public Produto saida() {
		scanner = new Scanner(System.in);
		produtoService.relatorioestoque();
		System.out.print("Digite o c�digo do produto da lista para efetuar sa�da: ");
		Produto produto = produtoService.obterProdutoPeloCodigo(scanner.nextInt());
		System.out.print("Digite a quantidade do produto: ");
		int qtde = scanner.nextInt();
		if (qtde > produto.getSaldo()) {
			System.out.println(
					"Quantidade superior ao saldo de: " + produto.getSaldo() + " " + produto.getApresentacao() + "S");
			System.out.println("Refa�a o pedido!");
			saida();
		} else {
			produto.setSaldo(produto.getSaldo() - qtde);
			if (produto.getSaldo() < produto.getEstoqueMinimo()) {
				System.out.println("Estoque m�nimo de " + produto.getEstoqueMinimo() + " com d�ficit de "
						+ (produto.getEstoqueMinimo() - produto.getSaldo()) + " " + produto.getApresentacao() + "S");
			}
			System.out.println("Sa�da efetuada com sucesso!");
		}
		return produto;
	}
}

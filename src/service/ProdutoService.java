package service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import modelo.Produto;
import util.Uteis;

public class ProdutoService {

	private List<Produto> produtos;
	protected Scanner scanner;
	protected Produto produto;
	protected Uteis uteis;

	public void cadastraProduto(GrupoService grupoService) {
		this.scanner = new Scanner(System.in);
		this.produto = new Produto();
		System.out.print("Descri��o do produto: ");
		produto.setProduto(scanner.nextLine().toUpperCase());
		System.out.print("Apresenta��o do produto: ");
		produto.setApresentacao(scanner.nextLine().toUpperCase());
		System.out.print("Estoque m�nimo do produto: ");
		produto.setEstoqueMinimo(scanner.nextDouble());
		System.out.print("Estoque m�ximo do produto: ");
		produto.setEstoqueMaximo(scanner.nextDouble());
		grupoService.listarGrupos();
		System.out.println("C�digo do grupo do produto: ");
		produto.setGrupo(grupoService.obterGrupoPeloCodigo(scanner.nextInt()));

		adiciona(produto);

		scanner = null;
		System.out.println("Produto cadastrado com sucesso!");
	}

	public void editaProduto() {
		this.scanner = new Scanner(System.in);
		this.listarProdutos();
		System.out.print("Digite o c�digo do produto que deseja editar:");
		Produto produtotemp = obterProdutoPeloCodigo(scanner.nextInt());
		if (produtotemp != null) {
			System.out.println("Digite a nova descri��o do produto " + produtotemp.getProduto());
			this.scanner = new Scanner(System.in);
			String temp = scanner.nextLine();
			if (!temp.trim().equals("")) {
				getProdutos().get(produtotemp.getCodigo() - 1).setProduto(temp.toUpperCase());
				System.out.println("Produto alterado com sucesso");
			}
		} else {
			System.out.println("N�o foi possivel localizar/alterar produto!");
		}
		this.scanner = null;
	}

	public void removeProduto() {
		this.scanner = new Scanner(System.in);
		this.listarProdutos();
		System.out.println("Digite o c�digo do produto que deseja remover:");
		this.remover(this.obterProdutoPeloCodigo(scanner.nextInt()));
		this.scanner = null;
		System.out.println("Produto removido com sucesso!");
	}

	public void remover(Produto produto) {
		this.getProdutos().get(this.obterIndiceProduto(produto)).setCodigo(0);
	}

	public void adiciona(Produto produto) {
		produto.setCodigo(this.getProdutos().size() + 1);
		this.getProdutos().add(produto);
	}

	public int obterIndiceProduto(Produto produto) {
		for (int i = 0; i < produtos.size(); i++) {
			if (produto.getCodigo() == produtos.get(i).getCodigo()) {
				return i;
			}
		}
		return 0;
	}

	public Produto obterProdutoPeloCodigo(int codigo) {
		for (int i = 0; i < this.getProdutos().size(); i++) {
			if (codigo == this.getProdutos().get(i).getCodigo()) {
				return this.getProdutos().get(i);
			}
		}
		return null;
	}

	public List<Produto> getProdutos() {
		if (produtos == null) {
			produtos = new ArrayList<Produto>();
		}
		return produtos;
	}

	public void listarProdutos() {
		if (this.getProdutos().size() > 0) {
			System.out.println(
					"=====================================================================================================================");
			System.out.println("PRODUTOS CADASTRADOS");
			System.out.println(
					"=====================================================================================================================");
			for (Produto p : produtos) {
				if (p.getCodigo() > 0) {
					System.out.println("C�digo: " + p.getCodigo() + " - Produto: " + p.getProduto()
							+ " - Apresenta��o: " + p.getApresentacao() + " - Est. Min. " + p.getEstoqueMinimo()
							+ " - Est. M�x. " + p.getEstoqueMaximo() + " - Grupo: " + p.getGrupo().getGrupo());
				}
			}
			System.out.println(
					"=====================================================================================================================");
		} else {
			System.out.println("nenhum produto cadastrado!\n");
		}

	}

	public void relatorioestoque() {
		if (this.getProdutos().size() > 0) {
			System.out.println(
					"=====================================================================================================================");
			System.out.println("SISTEMA- MANAGER SUPPLY PRO -- vr. 1.0 - Posi��o de Estoque atual");
			System.out.println(
					"=====================================================================================================================");
			for (Produto p : produtos) {
				if (p.getSaldo() > 0) {
					System.out.println("C�digo: " + p.getCodigo() + " - Produto: " + p.getProduto()
							+ " - Apresenta��o: " + p.getApresentacao() + " - Grupo: " + p.getGrupo().getGrupo()
							+ " - �ltimo pre�o: R$ " + p.getPreco() + " - Saldo. " + p.getSaldo()
							+ " - Valor Total: R$ " + p.getSaldo() * p.getPreco());
				}
			}
			System.out.println(
					"=====================================================================================================================");
		} else {
			System.out.println("nenhum produto com saldo!\n");
		}
	}

	public void relatorioEstoquePDF() {
		String relatorio = "";
		if (this.getProdutos().size() > 0) {
			uteis = new Uteis();
			relatorio += "==========================================================================\n"
					+ "SISTEMA- MANAGER SUPPLY PRO -- vr. 1.0 - Posi��o de Estoque em: "
					+ uteis.obterDataFormatada() + "\n"
					+ "==========================================================================\n";
			for (Produto p : produtos) {
				if (p.getSaldo() > 0) {
					relatorio += "C�digo: " + p.getCodigo() + " - Produto: " + p.getProduto() + " - Apresenta��o: "
							+ p.getApresentacao() + " - Grupo: " + p.getGrupo().getGrupo() + " - �ltimo pre�o: R$ "
							+ p.getPreco() + " - Saldo. " + p.getSaldo() + " - Valor Total: R$ "
							+ p.getSaldo() * p.getPreco()
							+ "\n----------------------------------------------------------------------------------------------------------------------------\n";
				}
			}
			relatorio += "========================================================================\n";
			uteis.gerarPDF(relatorio);
		} else {
			System.out.println("nenhum produto com saldo!\n");
		}
	}

	public void atualizaProduto(Produto produto) {
		this.getProdutos().set(this.obterIndiceProduto(produto), produto);
	}
}

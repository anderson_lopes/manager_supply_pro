package util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class Uteis {

	public void gerarPDF(String conteudo) {
		Document document = new Document();
		try {
			File file = new File("C://manager_supply_pro");
			file.mkdir();
			PdfWriter.getInstance(document, new FileOutputStream(
					"C://manager_supply_pro//posicao_de_estoque_" + obterComplementoNomeArquivo() + ".pdf"));
			document.open();
			document.add(new Paragraph(conteudo));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		document.close();
	}

	public String obterComplementoNomeArquivo() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		return formatter.format(new Date()).replaceAll("[/ :]", "");
	}
	
	public String obterDataFormatada() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		return formatter.format(new Date());
	}

}

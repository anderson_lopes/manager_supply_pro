package view;

import java.util.Scanner;

import service.GrupoService;
import service.PedidoService;
import service.ProdutoService;

public class Estoque {

	protected int opcao;
	protected Scanner scanner;

	protected ProdutoService produtoService;
	protected GrupoService grupoService;
	protected PedidoService pedidoService;

	public Estoque() {
		this.scanner = new Scanner(System.in);
		this.produtoService = new ProdutoService();
		this.grupoService = new GrupoService();
		this.pedidoService = new PedidoService();
	}

	public void menuPrincipal() {
		this.opcao = 0;
		System.out.println("=========================================");
		System.out.println("SISTEMA- MANAGER SUPPLY PRO -- vr. 1.0");
		System.out.println("=========================================");
		System.out.println("1 - Produtos.");
		System.out.println("2 - Grupos.");
		System.out.println("3 - Pedido de suprimentos.");
		System.out.println("4 - Relat�rio em tela de produtos com saldo.");
		System.out.println("5 - Relat�rio em PDF de produtos com saldo.");
		System.out.println("0 - Encerrar sistema..");

		System.out.println("\nDigite uma op��o...");

		opcao = scanner.nextInt();

		switch (opcao) {
		case 1:
			menuProduto();
			break;
		case 2:
			menuGrupo();
			break;
		case 3:
			menuPedidoSuprimentos();
			break;
		case 4:
			relatorioSuprimentos();
			menuPrincipal();
			break;
		case 5:
			produtoService.relatorioEstoquePDF();
			menuPrincipal();
			break;
		case 0:
			encerrarSistema();
			break;
		default:
			System.out.println("Op��o inv�lida!");
			menuPrincipal();
		}
	}

	public void menuProduto() {
		if (grupoService.getGrupos().size() == 0) {
			System.out.println("Para acessar o cadastro de produtos deve-se cadstrar pelo menos um grupo de produtos.");
			menuPrincipal();
		} else {
			this.opcao = 0;

			System.out.println("===================================================");
			System.out.println("SISTEMA- MANAGER SUPPLY PRO -- CADASTRO DE PRODUTOS");
			System.out.println("===================================================");
			System.out.println("1 - Cadastrar Produto.");
			System.out.println("2 - Excluir Produto.");
			System.out.println("3 - Editar Produto.");
			System.out.println("4 - Listar Produtos.");
			System.out.println("9 - Voltar para o menu principal.");
			System.out.println("0 - Encerrar sistema..");

			System.out.println("\nDigite uma op��o...");

			opcao = scanner.nextInt();

			switch (opcao) {
			case 1:
				produtoService.cadastraProduto(grupoService);
				menuProduto();
				break;
			case 2:
				produtoService.removeProduto();
				menuProduto();
				break;
			case 3:
				produtoService.editaProduto();
				menuProduto();
				break;
			case 4:
				produtoService.listarProdutos();
				menuProduto();
				break;
			case 9:
				menuPrincipal();
				break;
			case 0:
				encerrarSistema();
				break;
			default:
				System.out.println("Op��o inv�lida!");
				menuProduto();
			}
		}
	}

	public void menuGrupo() {
		this.opcao = 0;
		System.out.println("============================================================");
		System.out.println("SISTEMA- MANAGER SUPPLY PRO -- CADASTRO DE GRUPO DE PRODUTOS");
		System.out.println("============================================================");
		System.out.println("1 - Cadastrsr Grupo.");
		System.out.println("2 - Excluir Grupo.");
		System.out.println("3 - Editar Grupo.");
		System.out.println("4 - Listar Grupos.");
		System.out.println("9 - Voltar para o menu principal.");
		System.out.println("0 - Encerrar sistema..");

		System.out.println("\nDigite uma op��o...");

		opcao = scanner.nextInt();

		switch (opcao) {
		case 1:
			grupoService.cadastraGrupo();
			menuGrupo();
			break;
		case 2:
			grupoService.removeGrupo();
			menuGrupo();
			break;
		case 3:
			grupoService.editaGrupo();
			menuGrupo();
			break;
		case 4:
			grupoService.listarGrupos();
			menuGrupo();
			break;
		case 9:
			menuPrincipal();
			break;
		case 0:
			encerrarSistema();
			break;
		default:
			System.out.println("Op��o inv�lida!");
			menuGrupo();
		}
	}

	public void menuPedidoSuprimentos() {
		this.opcao = 0;
		System.out.println("======================================================");
		System.out.println("SISTEMA- MANAGER SUPPLY PRO -- PEDIDO DE SUPRIMENTOS");
		System.out.println("======================================================");
		System.out.println("1 - Entrada.");
		System.out.println("2 - Sa�da.");
		System.out.println("3 - Relat�rio em tela de produtos com saldo.");
		System.out.println("4 - Relat�rio em PDF de produtos com saldo.");
		System.out.println("9 - Voltar para o menu principal.");
		System.out.println("0 - Encerrar sistema..");

		System.out.println("\nDigite uma op��o...");
		opcao = scanner.nextInt();

		switch (opcao) {
		case 1:
			pedidoService.executaPedido(produtoService, 'E');
			menuPedidoSuprimentos();
			break;
		case 2:
			pedidoService.executaPedido(produtoService, 'S');
			menuPedidoSuprimentos();
			break;
		case 3:
			relatorioSuprimentos();
			menuPedidoSuprimentos();
			break;
		case 4:
			produtoService.relatorioEstoquePDF();
			menuPedidoSuprimentos();
			break;
		case 9:
			menuPrincipal();
			break;
		case 0:
			encerrarSistema();
			break;
		default:
			System.out.println("Op��o inv�lida!!!");
			menuPedidoSuprimentos();
		}
	}

	public void relatorioSuprimentos() {
		produtoService.relatorioestoque();
	}

	public void encerrarSistema() {
		System.out.println("Sistema encerrado com sucesso!....");
		System.exit(0);
	}
}
